# Let's add internal IP addresses of CEPH servers

hostsfile_entry '10.42.1.11' do
    hostname  'ceph-mon1.stor.gitlab.com'
    aliases   ['ceph-mon1']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.12' do
    hostname  'ceph-mon2.stor.gitlab.com'
    aliases   ['ceph-mon2']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.13' do
    hostname  'ceph-mon3.stor.gitlab.com'
    aliases   ['ceph-mon3']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.51' do
    hostname  'ceph-osd1.stor.gitlab.com'
    aliases   ['ceph-osd1']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.52' do
    hostname  'ceph-osd2.stor.gitlab.com'
    aliases   ['ceph-osd2']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.53' do
    hostname  'ceph-osd3.stor.gitlab.com'
    aliases   ['ceph-osd3']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.54' do
    hostname  'ceph-osd4.stor.gitlab.com'
    aliases   ['ceph-osd4']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.55' do
    hostname  'ceph-osd5.stor.gitlab.com'
    aliases   ['ceph-osd5']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.56' do
    hostname  'ceph-osd6.stor.gitlab.com'
    aliases   ['ceph-osd6']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.57' do
    hostname  'ceph-osd7.stor.gitlab.com'
    aliases   ['ceph-osd7']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.58' do
    hostname  'ceph-osd8.stor.gitlab.com'
    aliases   ['ceph-osd8']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.59' do
    hostname  'ceph-osd9.stor.gitlab.com'
    aliases   ['ceph-osd9']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.60' do
    hostname  'ceph-osd10.stor.gitlab.com'
    aliases   ['ceph-osd10']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.61' do
    hostname  'ceph-osd11.stor.gitlab.com'
    aliases   ['ceph-osd11']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end

hostsfile_entry '10.42.1.62' do
    hostname  'ceph-osd12.stor.gitlab.com'
    aliases   ['ceph-osd12']
    comment   'Appended by gitlab-ceph::host-files'
    action    :append
end
