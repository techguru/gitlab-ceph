# Let's add the Ceph apt repository

include_recipe 'apt::default'

apt_repository 'ceph' do
    uri 'http://aptly.gitlab.com/'
    components ['main']
    distribution node['lsb']['codename']
    key 'http://aptly.gitlab.com/release.asc'
    action :add
    deb_src false
end
